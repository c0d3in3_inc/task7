package com.university.exam8

import android.app.Application
import android.content.Context
import android.content.res.Resources

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance : App
    }

    fun getContext(): Context = instance.applicationContext
}